drop table if exists unidb_teach;
drop table if exists unidb_attend;
drop table if exists unidb_courses;
drop table if exists unidb_lecturers;
drop table if exists unidb_students;


create table unidb_students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    primary key (id),
    check (country like '__')
);

create table unidb_lecturers (
    staff_no int not null,
    fname varchar(32),
    lname varchar(32),
    office char(8),
    primary key (staff_no)
);

create table unidb_courses (
    dept varchar(10) not null,
    num int not null,
    description text,
    coord_no int,
    rep_id int,
    primary key(dept, num),
    foreign key (coord_no) references unidb_lecturers (staff_no), -- the course co-ordinator must be an existing lecturer.
    foreign key (rep_id) references unidb_students (id) -- the student rep must be an existing student.
);

create table unidb_attend (
    student_id int not null,
    dept varchar(10) not null,
    num int not null,
    semester char(2),
    mark char(2),
    primary key (student_id, dept, num), -- each student can only be enrolled in a given subject once.
    foreign key (student_id) references unidb_students (id), -- only existing students can attend courses.
    foreign key (dept, num) references unidb_courses (dept, num) -- students can only attend existing courses.
);

create table unidb_teach (
    dept varchar(10) not null,
    num int not null,
    staff_no int not null,
    semester char(2) not null,
    primary key (dept, num, staff_no), -- each lecturer can only teach each subject once.
    foreign key (dept, num) references unidb_courses (dept, num), -- only existing courses can be taught.
    foreign key (staff_no) references unidb_lecturers (staff_no) -- only existing staff may teach courses.
);