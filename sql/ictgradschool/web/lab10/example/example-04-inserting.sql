-- Drop existing tables (to get rid of any "old" data)
-- ----------------------------------------------------------------------
drop table if exists unidb_teach;
drop table if exists unidb_attend;
drop table if exists unidb_courses;
drop table if exists unidb_lecturers;
drop table if exists unidb_students;
-- ----------------------------------------------------------------------


-- Create schema
-- ----------------------------------------------------------------------
create table unidb_students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    mentor int,
    primary key (id),
    check (country like '__')
);

create table unidb_lecturers (
    staff_no int not null,
    fname varchar(32),
    lname varchar(32),
    office char(8),
    primary key (staff_no)
);

create table unidb_courses (
    dept varchar(10) not null,
    num int not null,
    description text,
    coord_no int,
    rep_id int,
    primary key(dept, num),
    foreign key (coord_no) references unidb_lecturers (staff_no),
    foreign key (rep_id) references unidb_students (id)
);

create table unidb_attend (
    student_id int not null,
    dept varchar(10) not null,
    num int not null,
    semester char(2),
    mark char(2),
    primary key (student_id, dept, num),
    foreign key (student_id) references unidb_students (id),
    foreign key (dept, num) references unidb_courses (dept, num)
);

create table unidb_teach (
    dept varchar(10) not null,
    num int not null,
    staff_no int not null,
    primary key (dept, num, staff_no),
    foreign key (dept, num) references unidb_courses (dept, num),
    foreign key (staff_no) references unidb_lecturers (staff_no)
);
-- ----------------------------------------------------------------------


-- Add data to database
-- ----------------------------------------------------------------------

-- Adding lecturers
INSERT INTO unidb_lecturers (staff_no, fname, lname, office) VALUES
    (123, 'David', 'Bainbridge', 'G.1.24'),
    (456, 'Geoff', 'Holmes', 'FG.1.01'),
    (666, 'Annika', 'Hinze', 'G.2.26'), -- Add a lecturer with ID 666, named Annika Hinze, in office G.2.26
    (707, 'Te Taka', 'Keegan', 'G.2.07'),
    (878, 'Barney', 'Rubble', 'L.2.34'),
    (919, 'SpongeBob', 'Squarepants', 'L.3.13');

-- Adding students
INSERT INTO unidb_students (id, fname, lname, country, mentor) VALUES
    (1661, 'James', 'Beam', 'US', 1715),
    (1668, 'Jack', 'Daniels', 'US', 1715),
    (1713, 'James', 'Speight', 'NZ', 1715),
    (1715, 'Henry', 'Wagstaff', 'NZ', 1970),
    (1717, 'Johnnie', 'Walker', 'AU', 1715), -- Add a student with ID 1717, named Johnnie Walker, from Australia, who has Henry Wagstaff as a mentor.
    (1824, 'Tia', 'Maria', 'MX', 1970),
    (1828, 'Dom', 'Perignon', 'FR', 1970),
    (1970, 'Scarlett', 'Ohara', 'SC', 1970),
    (1971, 'Rob', 'Roy', 'SC', 1970);

-- Adding courses
INSERT INTO unidb_courses (dept, num, description, coord_no, rep_id) VALUES
    ('comp', '219', 'databases', 666, 1713),
    ('comp', '258', 'OOP', 707, 1970), -- Add a course with ID comp 258, called OOP, with Te Taka as a course co-ordinator, and Scarlett Ohara as a student rep.
    ('comp', '425', 'HCI', 123, 1717),
    ('ling', '219', 'Linguistics', 878, 1824),
    ('teal', '202', 'Film', 919, 1668);

-- Adding information about which students attend which courses
INSERT INTO unidb_attend (student_id, dept, num, semester, mark) VALUES
    (1661, 'comp', '219', 'A', 'A-'),
    (1661, 'comp', '258', 'A', 'B-'),
    (1668, 'comp', '219', 'A', 'B+'), -- State that Jack Daniels (student ID 1668) attended comp 219 in Semester A, and scored a B+.
    (1668, 'comp', '258', 'A', 'B+'),
    (1717, 'comp', '219', 'A', 'RP'),
    (1828, 'ling', '219', 'A', 'A-'),
    (1970, 'comp', '219', 'A', 'B+'),
    (1970, 'teal', '202', 'B', 'C+'),
    (1971, 'comp', '219', 'A', NULL);

-- Adding information about which lecturers teach which courses
INSERT INTO unidb_teach (dept, num, staff_no) VALUES
    ('comp', '219', 666), -- State that comp 219 is taught by Annika Hinze (staff ID 666)
    ('comp', '219', 707), -- comp 219 is also taught by Te Taka Keegan (staff ID 707)
    ('comp', '258', 707),
    ('ling', '219', 878),
    ('teal', '202', 919);

-- Errors!
-- insert into unidb_students values (1670, 'Not', 'Exist', 'Q', NULL); -- Won't work because "Q" is not a valid country code (they must have 2 characters).
-- insert into unidb_attend values (9999, 'comp', 219, 'B', 'D-'); -- Won't work because 9999 is not an id of an existing student.
-- insert into unidb_teach values ('cump', 219, 878); -- Won't work because "CUMPSCI" is not a valid course department.
-- ----------------------------------------------------------------------