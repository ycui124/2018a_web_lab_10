drop table if exists unidb_students;

-- NOTE: Unfortunately check constraints are NOT enabled by default on MySQL.
-- You will be able to run this successfully but won't notice a difference in behaviour.

create table unidb_students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    primary key (id),
    check (country like '__'), -- The country MUST contain exactly two characters (e.g. NZ, US, ...)
    check (id > 0 and id < 100000), -- The id must be between 1 and 100000 (inclusive).
    check (fname like '_____%') -- The fname must contain at least 5 characters.
);