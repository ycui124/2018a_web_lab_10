-- NOTE: Before running this file, run the example-04-inserting script to populate the database.

-- Querying the data
-- ----------------------------------------------------------------------

-- I want to get a list of all of the students. I want all data in the students table.
select *
from unidb_students;

-- I want to get the student id's and first names of all students.
select id, fname
from unidb_students;

-- I want to get all students from NZ.
select *
from unidb_students
where country = 'NZ';

-- I want to get all lecturers in G block
select *
from unidb_lecturers
where office like 'G.%';

-- I want to get the department and code of all courses students took in semester A.
select distinct dept, num -- "distinct" removes duplicate values in the result.
from unidb_attend
where semester = 'A';
-- ----------------------------------------------------------------------