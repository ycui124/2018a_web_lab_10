-- Answers to Exercise 9 here
SELECT * from lab10_ex03_table;

SELECT id,name,gender,year_born,joined FROM lab10_ex03_table;

SELECT * from lab10_ex04_table;

SELECT director from lab10_ex06_video_rental GROUP BY director;

SELECT movie_title,rate FROM lab10_ex06_video_rental WHERE rate<=2;

SELECT username FROM lab10_ex02_table ORDER BY username DESC;

SELECT first_name from lab10_ex02_table WHERE first_name LIKE 'Pete%';

SELECT first_name,last_name from lab10_ex02_table WHERE first_name LIKE '%Pete%' OR last_name LIKE '%Pete%';

