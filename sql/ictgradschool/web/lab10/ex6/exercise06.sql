-- Answers to Exercise 6 here
ALTER TABLE lab10_ex03_table add id int PRIMARY KEY AUTO_INCREMENT;


CREATE TABLE lab10_ex06_video_rental(
  id CHAR(8),
  movie_title VARCHAR(100),
  director VARCHAR(50),
  rate INT,
  loan_member INT,
  FOREIGN KEY (loan_member) REFERENCES lab10_ex03_table(id)
);


INSERT INTO lab10_ex06_video_rental VALUES (11111111,'aaaaa','aaa',2,1);
INSERT INTO lab10_ex06_video_rental VALUES (22222222,'bbbbb','bbb',6,2);
INSERT INTO lab10_ex06_video_rental VALUES (33333333,'ccccc','ccc',4,1);
INSERT INTO lab10_ex06_video_rental VALUES (44444444,'ddddd','ddd',2,1);
INSERT INTO lab10_ex06_video_rental VALUES (55555555,'fffff','aaa',2,1);
INSERT INTO lab10_ex06_video_rental VALUES (66666666,'eeeee','aaa',2,1);
INSERT INTO lab10_ex06_video_rental VALUES (77777777,'ttttt','bbb',2,1);
INSERT INTO lab10_ex06_video_rental VALUES (888,'ddddddddd','bbb',4,3);
INSERT INTO lab10_ex06_video_rental VALUES (8654,'wwwww','rrr',6,4);
INSERT INTO lab10_ex06_video_rental VALUES (8654,'wwwww','rrr',6,3);


DROP TABLE lab10_ex06_members;
DROP TABLE lab10_ex06_video_rental;