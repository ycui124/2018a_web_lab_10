-- Answers to Exercise 3 here
CREATE TABLE lab10_ex03_table(
  name VARCHAR(100),
  gender VARCHAR(6),
  year_born INT,
  joined INT,
  num_hires INT
);
INSERT INTO lab10_ex03_table VALUES ('Peter Jackson','male',1961,1997,17000);
INSERT INTO lab10_ex03_table VALUES ('Jane Campion','female',1954,1980,30000);
INSERT INTO lab10_ex03_table VALUES ('Roger Donaldson','male',1945,1980,12000);
INSERT INTO lab10_ex03_table VALUES ('Temuera Morrison','male',1960,1995,15500);
INSERT INTO lab10_ex03_table VALUES ('Russell Crowe','male',1964,1990,10000);
INSERT INTO lab10_ex03_table VALUES ('Lucy Lawless','female',1968,1995,5000);
INSERT INTO lab10_ex03_table VALUES ('Michael Hurst','male',1957,2000,15000);
INSERT INTO lab10_ex03_table VALUES ('Andrew Niccol','male',1964,1997,3500);
INSERT INTO lab10_ex03_table VALUES ('Kiri Te Kanawa','female',1944,1997,500);
INSERT INTO lab10_ex03_table VALUES ('Lorde','female',1996,2010,1000);