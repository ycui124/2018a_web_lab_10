-- Answers to Exercise 4 here
CREATE TABLE lab10_ex04_table(
  id INT,
  title VARCHAR(100),
  content TEXT,
  PRIMARY KEY (id)
);
INSERT INTO lab10_ex04_table VALUES (1,'What is "Lorem ipsum"?','n publishing and graphic design, lorem ipsum is common placeholder text used to demonstrate the graphic elements of a document or visual presentation, such as web pages, typography, and graphical layout. It is a form of "greeking".');
INSERT INTO lab10_ex04_table VALUES (2,'Where can I get some?','On this page for example. Just click button "Generate".');