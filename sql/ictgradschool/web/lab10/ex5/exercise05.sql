-- Answers to Exercise 5 here
CREATE TABLE lab10_ex05_table (
  username VARCHAR(20) NOT NULL,
  first_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(100),
  PRIMARY KEY (username)
);
INSERT INTO lab10_ex05_table VALUES ('programmer1', 'Bill','Gates','bill@microsoft.com'),
  ('programmer2','Peter','Peter','peter@microsoft.com');
INSERT INTO lab10_ex05_table VALUES ('programmer3', 'Pete','Gates','pete@microsoft.com'),
  ('programmer4','Garry','Peterson','peterpeterson@microsoft.com');


INSERT INTO lab10_ex05_table VALUES ('programmer3', 'Pete','Gates','pete@microsoft.com');